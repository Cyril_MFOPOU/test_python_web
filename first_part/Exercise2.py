def colorful(number):
    if (number < 0):
        return 'Error,' + str(number) + ' is not positive.Please change the value of number'
    else:
        decomposition_list = []
        dict_product = {}
        number_str = str(number)
        #decomposition of number:  number = 263 , result : ['2', '26', '263', '6', '63', '3']
        for i in range(len(number_str)):
            for j in range(i, len(number_str)):
                decomposition_list.append(number_str[i:j + 1])
        #dictionary of product of all consecutive subsets of digits. For the last list we've {0: 2, 1: 2x6, 2: 2x6x3, 3: 6, 4: 6x3, 5: 3}
        for p in range(len(decomposition_list)):
            t = 1
            x = decomposition_list[p]
            for j in range(len(x)):
                t *= int(x[j])
            dict_product[p] = t
      
        # final verification
        table_verification = []

        for m, n in dict_product.items():
            if n not in table_verification:
                table_verification.append(n)
        if len(table_verification) == len(decomposition_list):
            return str(number) + " is a colorful number"
        else:
            return str(number) + " isn't a colorful number"

