def exercise_one():
    for i in range(1,101):
        if i % 3 == 0 and i % 5 != 0:
            print('Three')
        elif i % 5 == 0 and i % 3 != 0:
            print('Five')
        elif i % 5 == 0 and i % 3 == 0:
            print('ThreeFive')
        else:
            print(i)




