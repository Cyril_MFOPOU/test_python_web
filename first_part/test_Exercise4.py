import pytest

from first_part.Exercise4 import anagrams

def test_to_anagrams():
    assert anagrams('abba', ['aabb', 'abcd', 'bbaa', 'dada']) == ['aabb', 'bbaa']
    assert anagrams('racer', ['crazer', 'carer', 'racar', 'caers', 'racer']) == ['carer', 'racer']
    assert anagrams('laser', ['lazing', 'lazy',  'lacer']) ==  []
