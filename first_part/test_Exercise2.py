import pytest

from first_part.Exercise2 import colorful

def test_to_colorful():
    assert colorful(-263) == 'Error,-263 is not positive.Please change the value of number'
    assert colorful(263) == "263 is a colorful number"
    assert colorful(236) == "236 isn't a colorful number"
    assert colorful(2532) == "2532 isn't a colorful number"