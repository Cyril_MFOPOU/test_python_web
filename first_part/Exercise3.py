def calculate(list_string):
    if(not isinstance(list_string,list)):
        return False
    else:
        s = 0
        for i in range(len(list_string)):
            if ( not isinstance(list_string[i], str)):
                continue
            if(list_string[i].lstrip('-').isdigit()):
                s += int(list_string[i])
        return s

