import pytest

from first_part.Exercise3 import calculate

def test_to_calculate():
    assert calculate(['4', '3', '-2']) == 5
    assert calculate('54') == False
    assert calculate(['nothing', 3, '8', 2, '1']) == 9
    assert calculate(453) == False
