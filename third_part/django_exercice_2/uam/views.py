from django.shortcuts import render
from django.template import loader
from django.http import HttpResponse
from django.http import JsonResponse
from .models import User,Company,Table,Tabs

# Create your views here.
def index(request):
    template = loader.get_template('index.html')
    user = User.objects.get(id=1)
    liste = []
    tabs_keys = ['id','name']
    tabs_values = []
    for i in range(len(user.tabs.all())):
        tabs_values.append(i+1)
        tabs_values.append(user.tabs.all()[i].name)
        liste.append(dict(zip(tabs_keys,tabs_values)))
        tabs_values = []

    data = {"first_name": user.first_name,"last_name":user.last_name,"email":user.email,"company":user.company.name,"tabs":liste}
    return HttpResponse(template.render(data))