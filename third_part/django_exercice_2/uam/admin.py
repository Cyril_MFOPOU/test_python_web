from django.contrib import admin
from .models import User,Company,Table,Tabs

admin.site.register(User)
admin.site.register(Company)
admin.site.register(Table)
admin.site.register(Tabs)