from django.db import models


class User(models.Model):
    first_name = models.CharField(max_length=256)
    last_name = models.CharField(max_length=256)
    email = models.EmailField(max_length=256)
    table = models.ForeignKey('Table', on_delete=models.CASCADE, blank=True, null=True)
    company = models.ForeignKey('Company', on_delete=models.CASCADE, blank=True, null=True)
    tabs = models.ManyToManyField('Tabs')

    def __str__(self):
        return f'{self.first_name},{self.last_name}'


class Company(models.Model):
    name = models.CharField(max_length=256)

    def __str__(self):
        return self.name

class Table(models.Model):
    name = models.CharField(max_length=256)

    def __str__(self):
        return self.name

class Tabs(models.Model):
    name = models.CharField(max_length=256)

    def __str__(self):
        return self.name





