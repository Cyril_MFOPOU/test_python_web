from django.shortcuts import render
from django.template import loader
from django.http import HttpResponse
from django.http import JsonResponse
from .models import Officer, Captain, Rank, Ship

# Create your views here.

# Create your views here.
def index_1(request):
    ships = []
    count = Ship.objects.count()
    for i in range(count):
        ships.append({"name":Ship.objects.get(id=i+1).name,"captain":Ship.objects.get(id=i+1).captain.name})
    return JsonResponse({'ships': ships})

def index_2(request):
    officers = []
    ship2 = Ship.objects.get(id=2)
    count = Officer.objects.count()
    for i in range(count):
        off = Officer.objects.get(id=i+1)
        if(off.ship_assignment == ship2):
            officers.append({"name":Officer.objects.get(id=i+1).name,"order":off.order})

    return JsonResponse({'officers': officers})

def index_3(request):
    officers = []
    #ship2 = Ship.objects.get(id=2)
    count = Officer.objects.count()
    #liste = []
    for i in range(count):
        liste = []
        off = Officer.objects.get(id=i+1)
        if(Rank.objects.get(name = "Commander") in off.rank.all()):
            for p in range(len(off.rank.all())):
                    liste.append(off.rank.all()[p].name)
            officers.append({"name":off.name,"order":off.order,"rank":liste})

    return JsonResponse({'officers': officers})