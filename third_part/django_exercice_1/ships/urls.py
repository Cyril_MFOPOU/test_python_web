from django.urls import path
from . import views

urlpatterns = [
    #path("", views.index_1,name="index_1"),
    path("view1/", views.index_1,name="index_1"),
    path("view2/", views.index_2,name="index_2"),
    path("view3/", views.index_3,name="index_3"),
]