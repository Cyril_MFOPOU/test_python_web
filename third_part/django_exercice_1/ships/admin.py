from django.contrib import admin
from .models import Officer, Captain, Rank, Ship

#
# class OfficerTabularInline(admin.TabularInline):
#     model = Officer
#     ordering = ('order',)
#     filter_horizontal = ('rank',)
#
#
# @admin.register(Officer)
# class OfficerAdmin(admin.ModelAdmin):
#     list_display = ('id', 'name',)
#
#
# @admin.register(Ship)
# class ShipAdmin(admin.ModelAdmin):
#     inlines = (OfficerTabularInline,)
#     list_display = ('name', 'registry')
#
#
# @admin.register(Rank)
# class RankAdmin(admin.ModelAdmin):
#     list_display = ('name',)

admin.site.register(Officer)
admin.site.register(Captain)
admin.site.register(Rank)
admin.site.register(Ship)
